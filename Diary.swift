

import Foundation
import RealmSwift

class Diary: Object {
    @objc dynamic var date = ""
    @objc dynamic var context = ""
    
    override static func primaryKey() -> String {
        return "date"
    }
}
